package com.example.seprojectapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.media.MediaPlayer

class MusicActivity : AppCompatActivity() {

    var mMediaPlayer: MediaPlayer? = null
    var mMediaPlayer2: MediaPlayer? = null
    var mMediaPlayer3: MediaPlayer? = null
    var mMediaPlayer4: MediaPlayer? = null
    var mMediaPlayer5: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music)
    }

    fun playSoundRain(view: View){
        if (mMediaPlayer == null){
            mMediaPlayer = MediaPlayer.create(this, R.raw.rain)
            mMediaPlayer!!.isLooping = true
            mMediaPlayer!!.start()
        } else mMediaPlayer!!.start()
    }

    fun pauseSoundRain(view: View){
        if (mMediaPlayer != null && mMediaPlayer!!.isPlaying) mMediaPlayer!!.pause()
    }

    fun playSoundOcean(view: View){
        if (mMediaPlayer2 == null){
            mMediaPlayer2 = MediaPlayer.create(this, R.raw.ocean)
            mMediaPlayer2!!.isLooping = true
            mMediaPlayer2!!.start()
        } else mMediaPlayer2!!.start()
    }

    fun pauseSoundOcean(view: View){
        if (mMediaPlayer2 != null && mMediaPlayer2!!.isPlaying) mMediaPlayer2!!.pause()
    }

    fun playSoundNature(view: View){
        if (mMediaPlayer3 == null){
            mMediaPlayer3 = MediaPlayer.create(this, R.raw.naturebirds)
            mMediaPlayer3!!.isLooping = true
            mMediaPlayer3!!.start()
        } else mMediaPlayer3!!.start()
    }

    fun pauseSoundNature(view: View){
        if (mMediaPlayer3 != null && mMediaPlayer3!!.isPlaying) mMediaPlayer3!!.pause()
    }

    fun playSoundStorm(view: View){
        if (mMediaPlayer4 == null){
            mMediaPlayer4 = MediaPlayer.create(this, R.raw.thunderstorm)
            mMediaPlayer4!!.isLooping = true
            mMediaPlayer4!!.start()
        } else mMediaPlayer4!!.start()
    }

    fun pauseSoundStorm(view: View){
        if (mMediaPlayer4 != null && mMediaPlayer4!!.isPlaying) mMediaPlayer4!!.pause()
    }

    fun playSoundShop(view: View){
        if (mMediaPlayer5 == null){
            mMediaPlayer5 = MediaPlayer.create(this, R.raw.coffee)
            mMediaPlayer5!!.isLooping = true
            mMediaPlayer5!!.start()
        } else mMediaPlayer5!!.start()
    }

    fun pauseSoundShop(view: View){
        if (mMediaPlayer5 != null && mMediaPlayer5!!.isPlaying) mMediaPlayer5!!.pause()
    }

    override fun onStop() {
        super.onStop()
        if (mMediaPlayer != null){
            mMediaPlayer!!.release()
            mMediaPlayer = null
        }
    }
}