package com.example.seprojectapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.seprojectapp.adapters.ExerciseAdapter
import com.example.seprojectapp.databinding.ActivityExerciseBinding
import com.example.seprojectapp.models.Exercise
import com.example.seprojectapp.viewmodels.ExerciseActivityViewModel

class ExerciseActivity : AppCompatActivity() {

    val TAG = "ABC"
    lateinit var binding: ActivityExerciseBinding
    private val vm: ExerciseActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_exercise)

        vm.fetchExerciseFromAPI()

        binding.rvExercises.layoutManager = LinearLayoutManager(this)
        //val adapter = TherapistsAdapter(therapistsList)
        //binding.rvTherapists.adapter = adapter



        val exerciseDataChanged = Observer<List<Exercise>>{
            if (!it.isNullOrEmpty()) {
                binding.rvExercises.adapter = ExerciseAdapter(it)
            }
        }

        vm.exerciseList.observe(this, exerciseDataChanged)

    }
}