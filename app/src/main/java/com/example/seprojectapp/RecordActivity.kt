package com.example.seprojectapp

import android.os.Bundle
import androidx.activity.viewModels
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.seprojectapp.adapters.RecordsAdapter
import com.example.seprojectapp.databinding.ActivityRecordBinding
import com.example.seprojectapp.models.SinglePaymentRecord
import com.example.seprojectapp.viewmodels.RecordViewModel

class RecordActivity : AppCompatActivity() {

    lateinit var binding: ActivityRecordBinding
    private val vm:RecordViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_record)

        vm.fetchRecordFromAPI()


        binding.rvRecords.layoutManager = LinearLayoutManager(this)

        val recordsDataChanged = Observer<List<SinglePaymentRecord>>{
            if (!it.isNullOrEmpty()){
                binding.rvRecords.adapter = RecordsAdapter(it)
            }
        }

        vm.recordList.observe(this, recordsDataChanged)

    }
}