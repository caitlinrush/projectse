package com.example.seprojectapp

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.seprojectapp.databinding.ActivityMeditationBinding


class MeditationActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMeditationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //setup the view binding variable to load the main activity.xml file
        binding = DataBindingUtil.setContentView(this, R.layout.activity_meditation)


        binding.btnCofirm.setOnClickListener(){
            Toast.makeText(this, "Meditation Preference Confirmed!", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }



    }
}