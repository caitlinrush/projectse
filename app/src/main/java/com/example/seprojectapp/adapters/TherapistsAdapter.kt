package com.example.seprojectapp.adapters


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.seprojectapp.models.Therapist
import com.example.seprojectapp.databinding.SingleTherapistRowLayoutBinding

/**
 * SEProjectApp created by caitlinrush
 * student ID : 991534296
 * on 2021-04-15 */

//@TODO: Make sure you convert the row_layout to a data binding layout!
class TherapistViewHolder(private val binding: SingleTherapistRowLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
    // @TODO: Create a bind() function that accepts a movie param
    fun bind(m: Therapist) {
        binding.theTherapist = m
        //a binding is usually asynchronous
        binding.executePendingBindings()
    }
}
class TherapistsAdapter (private val mTherapists: List<Therapist>) : RecyclerView.Adapter<TherapistViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TherapistViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        var binding = SingleTherapistRowLayoutBinding.inflate(inflater)
        // Return a new holder instance
        return TherapistViewHolder(binding)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: TherapistViewHolder, position: Int) {
        // Get the data model based on position
        val therapist: Therapist = mTherapists.get(position)
        viewHolder.bind(therapist)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mTherapists.size
    }
}