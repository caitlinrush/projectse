package com.example.seprojectapp.adapters

import android.graphics.Movie
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.seprojectapp.databinding.SingleRecordLayoutBinding
import com.example.seprojectapp.models.SinglePaymentRecord

/*
 * SEProjectApp created by yuanwang
 * student ID: 991470659
 * on 2021-04-16
 */
class RecordViewHolder(private val binding: SingleRecordLayoutBinding ) : RecyclerView.ViewHolder(binding.root) {
    // @TODO: Create a bind() function that accepts a movie param
    fun bind(r: SinglePaymentRecord) {
        binding.bndRecord = r
        binding.executePendingBindings()
    }
}
class RecordsAdapter (private val mRecords: List<SinglePaymentRecord>) : RecyclerView.Adapter<RecordViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecordViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        var binding = SingleRecordLayoutBinding.inflate(inflater)
        // Return a new holder instance
        return RecordViewHolder(binding)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: RecordViewHolder, position: Int) {
        // Get the data model based on position
        var record: SinglePaymentRecord = mRecords.get(position)
        viewHolder.bind(record)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mRecords.size
    }
}