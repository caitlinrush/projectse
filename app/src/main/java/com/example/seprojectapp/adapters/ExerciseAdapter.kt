package com.example.seprojectapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.seprojectapp.databinding.SingleExerciseRowLayoutBinding
import com.example.seprojectapp.databinding.SingleTherapistRowLayoutBinding
import com.example.seprojectapp.models.Exercise
import com.example.seprojectapp.models.Therapist

/**
 * SEProjectApp created by Pallavi Singla
 * Student ID : 991524414
 * on 2021-04-17 */
class ExerciseViewHolder(private val binding: SingleExerciseRowLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
    // @TODO: Create a bind() function that accepts a movie param
    fun bind(m: Exercise) {
        binding.theExercise = m
        //a binding is usually asynchronous
        binding.executePendingBindings()
    }
}
class ExerciseAdapter (private val mExercises: List<Exercise>) : RecyclerView.Adapter<ExerciseViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        var binding = SingleExerciseRowLayoutBinding.inflate(inflater)
        // Return a new holder instance
        return ExerciseViewHolder(binding)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ExerciseViewHolder, position: Int) {
        // Get the data model based on position
        val exercise: Exercise = mExercises.get(position)
        viewHolder.bind(exercise)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mExercises.size
    }
}