package com.example.seprojectapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.seprojectapp.databinding.ActivityTherapySearchScreenBinding

class TherapySearchScreen : AppCompatActivity() {


    lateinit var binding: ActivityTherapySearchScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTherapySearchScreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.back.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        binding.chooseBtn.setOnClickListener {
            val intent = Intent(this, TherapistActivity::class.java)
            startActivity(intent)
        }


        var testList = mutableListOf<model>()
        testList.add(model("Dr. Iplier", "Licensed ASMR therapist"))
        testList.add(model("Dr. Iplier", "Licensed ASMR therapist"))
        testList.add(model("Dr. Iplier", "Licensed ASMR therapist"))





    }
}