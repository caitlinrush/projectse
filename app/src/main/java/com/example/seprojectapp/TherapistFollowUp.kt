package com.example.seprojectapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.seprojectapp.databinding.ActivityTherapistFollowUpBinding
import com.example.seprojectapp.databinding.ActivityTherapySearchScreenBinding

class TherapistFollowUp : AppCompatActivity() {

    lateinit var binding: ActivityTherapistFollowUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTherapistFollowUpBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnMenu.setOnClickListener {
            val intent = Intent(this, MainActivityTherapist::class.java)
            startActivity(intent)
        }

    }
}