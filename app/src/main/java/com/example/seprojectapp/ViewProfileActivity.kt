package com.example.seprojectapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.seprojectapp.databinding.ActivityMainBinding
import com.example.seprojectapp.databinding.ActivityViewProfileBinding

class ViewProfileActivity : AppCompatActivity() {
    lateinit var binding:ActivityViewProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewProfileBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.backProfile.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            //for the time being the submit button goes to the Home screen, i.e. the Start screen of the app.
            startActivity(intent)
        }


    }
}