package com.example.seprojectapp.models

/**
 * SEProjectApp created by Pallavi Singla
 * Student ID : 991524414
 * on 2021-04-17 */
data class Exercise(
    val day:String,
    val type:String,
    val duration:String
)