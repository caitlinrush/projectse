package com.example.seprojectapp.models

/*
 * SEProjectApp created by yuanwang
 * student ID: 991470659
 * on 2021-04-16
 */

data class PaymentRecords(
    var results: List<SinglePaymentRecord>
) {
}