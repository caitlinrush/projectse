package com.example.seprojectapp.models

/**
 * SEProjectApp created by caitlinrush
 * student ID : 991534296
 * on 2021-04-15 */
data class Therapist (
        val name:String,
        val type:String,
        val email:String,
        val phone:String
){

}