package com.example.seprojectapp.models

/*
 * SEProjectApp created by yuanwang
 * student ID: 991470659
 * on 2021-04-16
 */
data class SinglePaymentRecord (
    var patient_id: Int,
    var therapist_id: Int,
    var record_id: Int,
    var session: SessionTracker,
    var amount: Float,
    var therapist_payable_amount: Float,
    var payment_method: Int,
    var success_payment_delivery: Boolean
        ){
}