package com.example.seprojectapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.seprojectapp.databinding.ActivityMainBinding
import com.example.seprojectapp.databinding.ActivityMainTherapistBinding

class MainActivityTherapist : AppCompatActivity() {
    lateinit var binding: ActivityMainTherapistBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //setup the view binding variable to load the main activity.xml file
        binding = ActivityMainTherapistBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnFollowUpTherapist.setOnClickListener {
            val intent = Intent(this, TherapistFollowUp::class.java)
            startActivity(intent)
        }

        binding.btnViewProfile.setOnClickListener {
            val intent = Intent(this, ViewProfileActivity::class.java)
            startActivity(intent)
        }

    }
}