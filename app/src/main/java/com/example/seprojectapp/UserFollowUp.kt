package com.example.seprojectapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.seprojectapp.databinding.ActivityTherapistFollowUpBinding
import com.example.seprojectapp.databinding.ActivityUserFollowUpBinding

class UserFollowUp : AppCompatActivity() {
    //lateinit var binding: ActivityUserFollowUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_follow_up)

        val btnExercise = findViewById<Button>(R.id.btnExerciseUser)

        val btnMoodTracker = findViewById<Button>(R.id.btnMoodTracker)

        btnExercise.setOnClickListener {
            val intent = Intent(this, ExerciseActivity::class.java)
            startActivity(intent)
        }

        btnMoodTracker.setOnClickListener {
            val intent = Intent(this, MoodtrackerActivity::class.java)
            startActivity(intent)
        }


    }
}