package com.example.seprojectapp.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.seprojectapp.apis.ExerciseAPI
import com.example.seprojectapp.models.Exercise
import kotlinx.coroutines.launch
import java.lang.Exception

/**
 * SEProjectApp created by Pallavi Singla
 * Student ID : 991524414
 * on 2021-04-17 */
class ExerciseActivityViewModel: ViewModel() {

    val TAG = "ABC-VM"

    var exerciseList = MutableLiveData<List<Exercise>>()

    fun fetchExerciseFromAPI() {
        viewModelScope.launch {
            try {
                // 1. use retrofit to call the api endpoint
                val response = ExerciseAPI.retrofitService.getExerciseFromAPI()
                // 2. check that you got a successful response
                if (response.isSuccessful && response.body() != null) {
                    exerciseList.value = response.body()
                }
                else {
                    // 4. if no, output a failure message
                    Log.d(TAG, "Error occured when getting the response from the API")
                }
            }
            catch(e: Exception) {
                Log.d(TAG, "Error: ${e.message}")
            }
        }
    }

}