package com.example.seprojectapp.viewmodels
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.seprojectapp.apis.RecordsApi
import com.example.seprojectapp.models.SinglePaymentRecord

import kotlinx.coroutines.launch



/*
 * SEProjectApp created by yuanwang
 * student ID: 991470659
 * on 2021-04-16
 */
class RecordViewModel(): ViewModel(){
    val TAG = "ABC"

    var recordList = MutableLiveData<List<SinglePaymentRecord>>()

    fun fetchRecordFromAPI(){

        //viewmodelscope is life cycle aware, auto manage background task
        viewModelScope.launch {
            try {
                //retrofit do all the api fetching and sending
                val response = RecordsApi.retrofitService.getRecords()
                if (response.isSuccessful && response.body() !=null){
                    val data = response.body()
                    recordList.value = data
                    Log.d(TAG, "records fetched")
                }else{
                    Log.d(TAG, "error fetching api")
                }
            }catch (e: Exception){
                Log.d(TAG," error ${e.message}")
            }

        }
    }


}