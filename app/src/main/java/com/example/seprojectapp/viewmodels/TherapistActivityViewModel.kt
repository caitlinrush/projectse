package com.example.seprojectapp.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.seprojectapp.TherapistAPI
import com.example.seprojectapp.models.Therapist
import kotlinx.coroutines.launch
import java.lang.Exception

/**
 * SEProjectApp created by caitlinrush
 * student ID : 991534296
 * on 2021-04-15 */
class TherapistActivityViewModel: ViewModel() {
    val TAG = "ABC-VM"

    var therapistList = MutableLiveData<List<Therapist>>()

    fun fetchTherapistFromAPI() {
        viewModelScope.launch {
            try {
                // 1. use retrofit to call the api endpoint
                val response = TherapistAPI.retrofitService.getTherapistFromAPI()
                // 2. check that you got a successful response
                if (response.isSuccessful && response.body() != null) {
                    therapistList.value = response.body()
                }
                else {
                    // 4. if no, output a failure message
                    Log.d(TAG, "Error occured when getting the response from the API")
                }
            }
            catch(e: Exception) {
                Log.d(TAG, "Error: ${e.message}")
            }
        }
    }
}