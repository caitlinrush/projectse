package com.example.seprojectapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.seprojectapp.adapters.TherapistsAdapter
import com.example.seprojectapp.databinding.ActivityTherapistBinding
import com.example.seprojectapp.models.Therapist
import com.example.seprojectapp.viewmodels.TherapistActivityViewModel
import kotlinx.coroutines.Job

class TherapistActivity : AppCompatActivity() {

    val TAG = "ABC"
    lateinit var binding:ActivityTherapistBinding
    private val vm:TherapistActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_therapist)

        vm.fetchTherapistFromAPI()

        //val therapistsList = mutableListOf<Therapist>()
        //therapistsList.add(Therapist("Test", "Test", "Test", "Test"))

        binding.rvTherapists.layoutManager = LinearLayoutManager(this)
        //val adapter = TherapistsAdapter(therapistsList)
        //binding.rvTherapists.adapter = adapter



        val therapistDataChanged = Observer<List<Therapist>>{
            if (!it.isNullOrEmpty()) {
                binding.rvTherapists.adapter = TherapistsAdapter(it)
            }
        }

        vm.therapistList.observe(this, therapistDataChanged)

    }


}

