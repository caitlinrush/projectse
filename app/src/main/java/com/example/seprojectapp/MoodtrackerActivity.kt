package com.example.seprojectapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

class MoodtrackerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_moodtracker)
    }

    fun ShowToast(view : View) {
        // make a toast and popup a message
        val t: Toast = Toast.makeText(this, "Your Responses have been saved!", Toast.LENGTH_SHORT);
        // show the toast on the screen.
        t.show()
    }

}