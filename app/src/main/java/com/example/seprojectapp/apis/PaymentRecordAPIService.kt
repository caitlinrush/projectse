package com.example.seprojectapp.apis

import com.example.seprojectapp.models.PaymentRecords
import com.example.seprojectapp.models.SinglePaymentRecord
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

/*
 * SEProjectApp created by yuanwang
 * student ID: 991470659
 * on 2021-04-16
 */
private const val BASE_URL = "https://gentle-oasis-80765.herokuapp.com"

// Initialize the http logging client, moshi, and retrofit
// --------------
// logging
private fun getHttpClient(): OkHttpClient {
    //val logging = HttpLoggingInterceptor()
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    val httpClient = OkHttpClient.Builder()
    httpClient.addInterceptor(logging)
    return httpClient.build()
}

// instantiate moshi
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

// instantiate retrofit
// @TODO: Uncomment the BASE url
private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .client(getHttpClient())
    .build()

interface APIService {

    @GET("/api/records")
    suspend fun getRecords(): Response<List<SinglePaymentRecord>>
}

// @TODO: Define an interface that will specify which API endpoints your app can connect to
//  - Interface has same name as this file

// @TODO: Uncomment when interface is done
// - Creates a singleton of your retrofit object.
// - There should only be 1 instance of retrofit throughout the entire application

object RecordsApi {
    val retrofitService : APIService by lazy {
        retrofit.create(APIService::class.java) }
}