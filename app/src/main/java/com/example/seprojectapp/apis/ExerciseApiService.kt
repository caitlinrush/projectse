package com.example.seprojectapp.apis

import com.example.seprojectapp.ApiService
import com.example.seprojectapp.models.Exercise
import com.example.seprojectapp.models.Therapist
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory.create
import retrofit2.http.GET

/**
 * SEProjectApp created by Pallavi Singla
 * Student ID : 991524414
 * on 2021-04-17 */


// @TODO: Add the base url of the API endpoint
private const val BASE_URL = "https://boiling-eyrie-68889.herokuapp.com"

// Initialize the http logging client, moshi, and retrofit
// --------------
// logging
private fun getHttpClient(): OkHttpClient {
    //val logging = HttpLoggingInterceptor()
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    val httpClient = OkHttpClient.Builder()
    httpClient.addInterceptor(logging)
    return httpClient.build()
}

// instantiate moshi
private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BASE_URL)
        .client(getHttpClient())
        .build()

// @TODO: Define an interface that will specify which API endpoints your app can connect to
//  - Interface has same name as this file
interface ExerciseApiService{
    @GET("/exercise/details")
    suspend fun getExerciseFromAPI(): Response<List<Exercise>>
}

// @TODO: Uncomment when interface is done
// - Creates a singleton of your retrofit object.
// - There should only be 1 instance of retrofit throughout the entire application

object ExerciseAPI {
    val retrofitService: ExerciseApiService by lazy {
        retrofit.create(ExerciseApiService::class.java)
    }
}