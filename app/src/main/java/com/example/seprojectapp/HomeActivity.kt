package com.example.seprojectapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity



class HomeActivity : AppCompatActivity() {

    //lateinit var binding:ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setup the view binding variable to load the main activity.xml file
        //binding = ActivityHomeBinding.inflate(layoutInflater)
        //val view = binding.root
        //setContentView(view)
        setContentView(R.layout.activity_home)

        val btnSearch = findViewById<Button>(R.id.btnSearch)
        val btnViewProfile = findViewById<Button>(R.id.btnViewProfile)
        val btnFollowUpUser = findViewById<Button>(R.id.btnFollowUpUser)
        val btnRecord = findViewById<Button>(R.id.btnRecord)
        val btnReceive = findViewById<Button>(R.id.btnReceive)
        val btnMeidation = findViewById<Button>(R.id.btnMeditation)

        btnSearch.setOnClickListener {
            val intent = Intent(this, TherapySearchScreen::class.java)
            startActivity(intent)
        }
        btnViewProfile.setOnClickListener {
            val intent = Intent(this, ViewProfileActivity::class.java)
            startActivity(intent)
        }

        btnReceive.setOnClickListener {
            val intent = Intent(this, MusicActivity::class.java)
            startActivity(intent)
        }

        btnRecord.setOnClickListener {
            val intent = Intent(this, RecordActivity::class.java)
            startActivity(intent)
        }

        btnFollowUpUser.setOnClickListener {
            val intent = Intent(this, UserFollowUp::class.java)
            startActivity(intent)
        }

        btnMeidation.setOnClickListener{
            val intent = Intent(this, MeditationActivity::class.java)
            startActivity(intent)
        }



    }
}